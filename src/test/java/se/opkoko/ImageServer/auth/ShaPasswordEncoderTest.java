package se.opkoko.ImageServer.auth;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class ShaPasswordEncoderTest {
    private Map<String, String> passwordsWithHashes;
    private ShaPasswordEncoder passwordEncoder;
    private String passwordHashNoPassword;
    private String passwordSalt;

    @Before
    public void setup() {
        passwordsWithHashes = ImmutableMap.of(
            "password1234567890", "c4ac4907f788a82788ce8ff53c4a260afa6bfba225541e33a4c94746bfa5be61",
            "sd7BMnJIg4Du", "b9ee0dd0df9fed8c01924945d4283f64d6522360b828cbe6150f3d5b075b36a8",
            "s3cre4P4ssw0rd", "804b82f8c4b7fa1338013bfd6ed2794c284b9499c4f158bd7bbe06e82947875e",
            "omegapoint", "7f7c718a837af3d2fdd0b262446807878b0573b74c08de8fd2fac293e175d9be",
            "opkoko2018.1", "5f37c17a6b436bb7e517c72d1675956dcff4b757fc237a5a5373b0d4de4b9a91");
        passwordEncoder = new ShaPasswordEncoder();
        passwordSalt = "aa";
        passwordHashNoPassword = "6eba133ffbec84d91133f251e8b645d74129c19da81b4f9eb6b47f6acd3c2920";
    }

    @Test
    public void shouldNotAcceptEmptyPasswords() throws Exception {
        assertFalse(passwordEncoder.matches("", passwordHashNoPassword));
    }

    @Test
    public void shouldNotAcceptNullPasswords() throws Exception {
        assertFalse(passwordEncoder.matches(null, passwordHashNoPassword));
    }

    @Test
    public void shouldConsiderValidPasswordsAsValid() throws Exception {
        for (Map.Entry<String, String> key : passwordsWithHashes.entrySet()) {
            assertTrue(passwordEncoder.matches(key.getKey(), key.getValue()));
        }

        // Just to make sure that we accept only the correct password
        assertFalse(passwordEncoder.matches("", passwordHashNoPassword));
        assertFalse(passwordEncoder.matches(null, passwordHashNoPassword));
        assertFalse(passwordEncoder.matches("password12345678901", passwordHashNoPassword));
        assertFalse(passwordEncoder.matches("password123456789", passwordHashNoPassword));
        assertFalse(passwordEncoder.matches(passwordSalt, passwordHashNoPassword));
        assertFalse(passwordEncoder.matches(passwordHashNoPassword, passwordHashNoPassword));
    }
}
