package se.opkoko.ImageServer.domain;

import org.junit.Before;
import org.junit.Test;
import se.opkoko.ImageServer.repository.creditcard.CreditCardEncryptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreditCardTest {

    private final static String CREDIT_CARD_HOLDER = "Emily Turner";
    private final static String CREDIT_CARD_EXPIRY_DATE = "08/2023";
    private final static String CREDIT_CARD_CVV_CODE = "991";

    private CreditCard creditCard;

    @Before
    public void setup() throws Exception {
        creditCard = new CreditCard();
        creditCard.setCardHolder(CREDIT_CARD_HOLDER);
        creditCard.setCvv(CREDIT_CARD_CVV_CODE);
        creditCard.setExp(CREDIT_CARD_EXPIRY_DATE);
    }


    @Test
    public void shouldGiveACreditCard() throws Exception {
        final String VISA_CREDIT_CARD_NUMBER = "4458523553575895";

        creditCard.setNumber(VISA_CREDIT_CARD_NUMBER);
        creditCard.setType(CreditCardType.VISA);

        assertNotNull(creditCard);
        assertEquals(CREDIT_CARD_HOLDER, creditCard.getCardHolder());
        assertEquals(CreditCardType.VISA, creditCard.getType());
        assertEquals(CREDIT_CARD_EXPIRY_DATE, creditCard.getExp());
        assertEquals(CREDIT_CARD_CVV_CODE, creditCard.getCvv());
        assertEquals(VISA_CREDIT_CARD_NUMBER, creditCard.getNumber());
    }

    @Test
    public void shouldDisplayAMaskedVisaCreditCard() throws Exception {
        final String VISA_CREDIT_CARD_NUMBER = "4458523553575895";
        final String MASKED_VISA_CREDIT_CARD_NUMBER = "445852******5895";

        creditCard.setNumber(VISA_CREDIT_CARD_NUMBER);
        creditCard.setType(CreditCardType.VISA);

        assertNotNull(creditCard);
        assertEquals(MASKED_VISA_CREDIT_CARD_NUMBER, creditCard.getDisplayNumber());

    }

    @Test
    public void shouldDisplayAMaskedMastercardCreditCard() throws Exception {
        final String MASTERCARD_CREDIT_CARD_NUMBER = "5431795957860482";
        final String MASKED_MASTERCARD_CREDIT_CARD_NUMBER = "543179******0482";

        creditCard.setNumber(MASTERCARD_CREDIT_CARD_NUMBER);
        creditCard.setType(CreditCardType.MASTERCARD);

        assertNotNull(creditCard);
        assertEquals(MASKED_MASTERCARD_CREDIT_CARD_NUMBER, creditCard.getDisplayNumber());

    }

    @Test
    public void shouldDisplayAMaskedAmericanExpressCreditCard() throws Exception {
        final String AMERICAN_EXPRESS_CREDIT_CARD_NUMBER = "371507246963891";
        final String MASKED_AMERICAN_EXPRESS_CREDIT_CARD_NUMBER = "37150******3891";

        creditCard.setNumber(AMERICAN_EXPRESS_CREDIT_CARD_NUMBER);
        creditCard.setType(CreditCardType.AMERICAN_EXPRESS);

        assertNotNull(creditCard);
        assertEquals(MASKED_AMERICAN_EXPRESS_CREDIT_CARD_NUMBER, creditCard.getDisplayNumber());

    }

    @Test
    public void shouldDisplayAMaskedAmericanExpressCreditCardAgain() throws Exception {
        final String AMERICAN_EXPRESS_CREDIT_CARD_NUMBER = "371507246963891";
        final String MASKED_AMERICAN_EXPRESS_CREDIT_CARD_NUMBER = "37150******3891";

        creditCard.setNumber(AMERICAN_EXPRESS_CREDIT_CARD_NUMBER);

        assertNotNull(creditCard);
        assertEquals(MASKED_AMERICAN_EXPRESS_CREDIT_CARD_NUMBER, creditCard.getDisplayNumber());

    }
}
