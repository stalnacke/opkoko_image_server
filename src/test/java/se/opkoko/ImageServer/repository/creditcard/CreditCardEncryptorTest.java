package se.opkoko.ImageServer.repository.creditcard;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import se.opkoko.ImageServer.domain.CreditCard;
import se.opkoko.ImageServer.domain.CreditCardType;

import static org.junit.Assert.*;

public class CreditCardEncryptorTest {

    private final static String creditCardString = "<creditCard>" +
        "<cardHolder>Emily Turner</cardHolder>" +
        "<cvv>991</cvv>" +
        "<exp>08/2023</exp>" +
        "<number>4458523553575895</number>" +
        "<type>VISA</type>" +
        "</creditCard>";
    private final static String CREDIT_CARD_HOLDER = "Emily Turner";
    private final static String CREDIT_CARD_EXPIRY_DATE = "08/2023";
    private final static String CREDIT_CARD_CVV_CODE = "991";
    private final static String CREDIT_CARD_NUMBER = "4458523553575895";
    private final static String secretTestKey = "abcd1234abcd1234";

    private CreditCardEncryptor creditCardEncryptor;

    @Before
    public void setUp(){
        creditCardEncryptor = new CreditCardEncryptor();
        ReflectionTestUtils.setField(creditCardEncryptor,"key", secretTestKey);
    }

    @Test
    public void shouldUnmarshalCreditCardIntoObject() throws Exception {
        CreditCard creditCard = creditCardEncryptor.unmarshal(creditCardString);

        assertNotNull(creditCard);
        assertEquals(CREDIT_CARD_HOLDER, creditCard.getCardHolder());
        assertEquals(CreditCardType.VISA, creditCard.getType());
        assertEquals(CREDIT_CARD_EXPIRY_DATE, creditCard.getExp());
        assertEquals(CREDIT_CARD_CVV_CODE, creditCard.getCvv());
        assertEquals(CREDIT_CARD_NUMBER, creditCard.getNumber());
    }

    @Test
    public void shouldMarshalCreditCardIntoObject() throws Exception {
        CreditCard creditCard = CreditCard.builder()
            .cardHolder(CREDIT_CARD_HOLDER)
            .cvv(CREDIT_CARD_CVV_CODE)
            .exp(CREDIT_CARD_EXPIRY_DATE)
            .number(CREDIT_CARD_NUMBER)
            .type(CreditCardType.VISA)
            .build();

       String creditCardXMLString = creditCardEncryptor.marshal(creditCard);

        assertNotNull(creditCardXMLString);
        assertEquals(creditCardXMLString, creditCardString);
    }

    @Test
    public void shouldDecryptCreditCards() throws Exception {
        CreditCard decryptedCreditCard = creditCardEncryptor.decryptCreditCard("3UNUm0UfeMVpLFclZGiGb/2PEV6xQ3jJVZkW741JcmMOz/5XFb6FgEkQswHbVveTMVvrP+rr+GZ5FGIns9oUH6SgBs8SG9itkqUf3icScedw+gzzcSgwi2TuZ+rjBGmxKI5Io4wn5N0viUcfNWeVQnBEg7K0k9AcumifyDuoFaHoDUL34w3tEXvSTZIgnQaQ+Ch2usrVvFwwjLPKpIEIQc5k3qllNpKgxrRS3ns6dz8=");

        CreditCard creditCard = CreditCard.builder()
            .cardHolder(CREDIT_CARD_HOLDER)
            .cvv(CREDIT_CARD_CVV_CODE)
            .exp(CREDIT_CARD_EXPIRY_DATE)
            .number(CREDIT_CARD_NUMBER)
            .type(CreditCardType.VISA)
            .build();

        assertEquals(creditCard.getCardHolder(), decryptedCreditCard.getCardHolder());
        assertEquals(creditCard.getCvv(), decryptedCreditCard.getCvv());
        assertEquals(creditCard.getExp(), decryptedCreditCard.getExp());
        assertEquals(creditCard.getType(), decryptedCreditCard.getType());
        assertEquals(creditCard.getNumber(), decryptedCreditCard.getNumber());
    }

    @Test
    public void decryptShouldBeReversable() throws Exception {

        CreditCard creditCard = CreditCard.builder()
            .cardHolder(CREDIT_CARD_HOLDER)
            .cvv(CREDIT_CARD_CVV_CODE)
            .exp(CREDIT_CARD_EXPIRY_DATE)
            .number(CREDIT_CARD_NUMBER)
            .type(CreditCardType.VISA)
            .build();

        String encryptCreditCard = creditCardEncryptor.encryptCreditCard(creditCard);
        CreditCard decryptedCreditCard = creditCardEncryptor.decryptCreditCard(encryptCreditCard);
        assertEquals(creditCard.getCardHolder(), decryptedCreditCard.getCardHolder());
        assertEquals(creditCard.getCvv(), decryptedCreditCard.getCvv());
        assertEquals(creditCard.getExp(), decryptedCreditCard.getExp());
        assertEquals(creditCard.getType(), decryptedCreditCard.getType());
        assertEquals(creditCard.getNumber(), decryptedCreditCard.getNumber());
    }

}
