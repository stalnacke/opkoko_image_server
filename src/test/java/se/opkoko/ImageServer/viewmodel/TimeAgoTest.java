package se.opkoko.ImageServer.viewmodel;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.*;

public class TimeAgoTest {

    private LocalDateTime now = LocalDateTime.of(2018, Month.MARCH, 4, 22, 0);

    @Before
    public void setup() {
    }

    @Test
    public void shouldHandleTimesCloseToNow() throws Exception {
        String relative = TimeAgo.toRelative(now, now.minusMinutes(1));
        assertEquals("nyss", relative);
    }

    @Test
    public void shouldHandleTimesInTheNearPast() throws Exception {
        String relative = TimeAgo.toRelative(now, now.minusMinutes(5).minusHours(3));
        assertEquals("3 timmar sedan", relative);
    }

    @Test
    public void shouldHandleTimesInTheNotSoNearPast() throws Exception {
        String relative = TimeAgo.toRelative(now, now.minusMinutes(5).minusHours(3).minusMonths(6));
        assertEquals("6 månader sedan", relative);
    }

    @Test
    public void shouldHandleTimesInTheLongAgo() throws Exception {
        String relative = TimeAgo.toRelative(now, now.minusMinutes(5).minusHours(3).minusMonths(6).minusYears(1));
        assertEquals("drygt ett år sedan", relative);
    }

    @Test
    public void shouldHandleTimesLongAgo() throws Exception {
        String relative = TimeAgo.toRelative(now, LocalDateTime.of(2016,06,06,10,22,00));
        assertEquals("nästan 2 år sedan", relative);
    }

}
