package se.opkoko.ImageServer.handler;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoCategory;
import se.opkoko.ImageServer.domain.autotagger.AutoTagger;
import se.opkoko.ImageServer.domain.autotagger.AutoTaggerTag;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class AutoTagServiceTest {
    private AutoTagService autoTagService;

    @Before
    public void setup() {
        autoTagService = new AutoTagService();
        ReflectionTestUtils.setField(autoTagService, "tagAuth", "password");
        ReflectionTestUtils.setField(autoTagService, "tagBackend", "https://ctf.opkoko.se/autotagger/");
    }

    @Test
    public void shouldPutMessageToServer() throws Exception {
        Photo photo = Photo.builder()
            .uuid("4A56CF57A75346CDBA6FE1876DA35877")
            .path("4A/56")
            .title("Bild")
            .category(PhotoCategory.builder().id(1L).name("Test").slug("test").build())
            .build();
        autoTagService.getTagsForPhoto(photo);
    }

    @Test
    public void shouldParseMessagesToAutoTaggerObjects() throws Exception {
        String response = "{" +
            "\"tags\": [{ \"name\": \"Red\", \"slug\": \"red\"}, { \"name\": \"Green\", \"slug\": \"green\"}]" +
        "}";

        AutoTagger autoTagger = autoTagService.convertStreamIntoAutoTagger(new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8)));
        assertTrue(autoTagger.getTags().contains(AutoTaggerTag.builder().name("Red").slug("red").build()));
        assertTrue(autoTagger.getTags().contains(AutoTaggerTag.builder().name("Green").slug("green").build()));
        assertEquals(2, autoTagger.getTags().size());
    }

}
