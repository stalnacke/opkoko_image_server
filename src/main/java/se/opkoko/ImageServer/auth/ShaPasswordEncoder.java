package se.opkoko.ImageServer.auth;

import com.google.common.hash.Hashing;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class ShaPasswordEncoder implements PasswordEncoder {

    private String encodePassword(String password) {
        return Hashing.sha256()
            .hashString(password, StandardCharsets.UTF_8)
            .toString();
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return this.encodePassword(rawPassword.toString());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword != null &&
            rawPassword != null &&
            this.encodePassword(rawPassword.toString()).equals(encodedPassword);
    }
}
