package se.opkoko.ImageServer.auth;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
