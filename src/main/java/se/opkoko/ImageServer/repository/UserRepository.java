package se.opkoko.ImageServer.repository;

import org.springframework.data.repository.Repository;
import se.opkoko.ImageServer.domain.User;


public interface UserRepository extends Repository<User, Long> {
    User findById(Long id);
    User findByUsername(String username);
}
