package se.opkoko.ImageServer.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

@Repository
public class HoneypotRepository {
    private final EntityManagerFactory entityManagerFactory;

    @Value("${images.table_prefix:}")
    private String tablePrefix;

    @Autowired
    public HoneypotRepository(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public boolean isBlocked(String fingerprint, String ip) {
        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            String sqlString = "SELECT count(*) FROM " + tablePrefix + "honeypot WHERE fingerprint = ?1 AND ip = ?2";
            BigInteger count = (BigInteger) session.createNativeQuery(sqlString)
                .setParameter(1, fingerprint)
                .setParameter(2, ip)
                .getSingleResult();
            return count.longValue() > 0;
        }
        catch (NoResultException e) {
            return false;
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }

    public void add(String fingerprint, String ip) {
        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            String sqlString = "INSERT INTO " + tablePrefix + "honeypot (fingerprint, ip, blocked_date) VALUES (?1, ?2, ?3)";
            EntityTransaction et = session.getTransaction();
            et.begin();
            session.createNativeQuery(sqlString)
                .setParameter(1, fingerprint)
                .setParameter(2, ip)
                .setParameter(3, new Date())
                .executeUpdate();
            et.commit();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }

    public void cleanup() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, -5);
        Date fiveMinutesAgo = cal.getTime();

        EntityManager session = entityManagerFactory.createEntityManager();
        EntityTransaction et = session.getTransaction();
        et.begin();
        try {
            String sqlString = "DELETE FROM " + tablePrefix + "honeypot WHERE blocked_date < ?1";
            session.createNativeQuery(sqlString)
                .setParameter(1, fiveMinutesAgo)
                .executeUpdate();
            et.commit();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }
}
