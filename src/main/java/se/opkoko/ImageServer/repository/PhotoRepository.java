package se.opkoko.ImageServer.repository;

import org.springframework.data.repository.Repository;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.User;

import java.util.List;


public interface PhotoRepository extends Repository<Photo, Long> {
    Photo findById(Long id);
    List<Photo> findByIdIn(List<Long> idList);
    List<Photo> findByOwner(User owner);
    Photo findByUuid(String uuid);
    List<Photo> findAll();

    Photo save(Photo photo);
}
