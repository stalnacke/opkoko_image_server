package se.opkoko.ImageServer.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import se.opkoko.ImageServer.domain.PhotoCategory;
import se.opkoko.ImageServer.domain.PhotoTag;
import se.opkoko.ImageServer.viewmodel.PhotoViewModel;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Repository
public class SearchRepository {
    private final EntityManagerFactory entityManagerFactory;

    private static Logger logger = LoggerFactory.getLogger(SearchRepository.class);

    @Value("${images.table_prefix:}")
    private String tablePrefix;

    @Autowired
    public SearchRepository(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public List<PhotoTag> getAllTags() {
        logger.debug("Loading all tags");

        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            String sqlString = "SELECT tag.id, tag.slug, tag.name FROM " + tablePrefix + "phototag AS tag ORDER BY tag.name";
            List resultList = session.createNativeQuery(sqlString, PhotoTag.class)
                .getResultList();

            logger.debug("Found a total of {} tags.", resultList.size());
            //noinspection unchecked
            return resultList;
        }
        catch (NoResultException e){
            return new ArrayList<>();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }

    private List<PhotoTag> getTags(long photoId) {
        logger.debug("Loading tags for {}", photoId);

        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            String sqlString = "SELECT tag.id, tag.slug, tag.name FROM " +
                tablePrefix + "photo_tag_rel AS rel LEFT OUTER JOIN " + tablePrefix +
                "phototag AS tag ON rel.tag_id = tag.id " +
                "WHERE rel.photo_id = " + photoId + " ORDER BY tag.name";
            List resultList = session.createNativeQuery(sqlString, PhotoTag.class)
                .getResultList();

            logger.debug("Found a total of {} tags.", resultList.size());
            //noinspection unchecked
            return resultList;
        }
        catch (NoResultException e){
            return new ArrayList<>();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }


    public List<PhotoTag> getAllTagsForCategory(PhotoCategory category) {
        logger.debug("Loading all tags for " + category);

        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            String sqlString = "SELECT DISTINCT tag.id, tag.slug, tag.name FROM " + tablePrefix + "phototag AS tag " +
                "LEFT OUTER JOIN " + tablePrefix + "photo_tag_rel AS rel ON tag.id = rel.tag_id " +
                "LEFT OUTER JOIN " + tablePrefix + "photo AS photo ON rel.photo_id = photo.id " +
                "WHERE photo.category_id = :category_id AND photo.approved = true ORDER BY tag.name";
            List resultList = session.createNativeQuery(sqlString, PhotoTag.class)
                .setParameter("category_id", category.getId())
                .getResultList();

            logger.debug("Found {} tags in category {}.", resultList.size(), category.getSlug());
            //noinspection unchecked
            return resultList;
        }
        catch (NoResultException e){
            return new ArrayList<>();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }

    private boolean convertSqlBoolToBool(Object o) {
    if (o instanceof Boolean) {
        return ((Boolean) o);
    }
    if (o instanceof BigInteger) {
        return ((BigInteger) o).longValue() != 0;
    }
    return false;
    }

    public List<PhotoViewModel> getPhotos(String category, List<String> tags) {
        logger.debug("Loading photos for " + category);

        String sqlString = "SELECT p.id, p.uuid, p.title, p.path, p.approved, p.photo_date, p.photographer FROM " + tablePrefix + "photo AS p";
        String whereString = " WHERE p.approved=true";

        if (category != null) {
            sqlString += " LEFT OUTER JOIN " + tablePrefix + "category AS c " +
                "ON p.category_id = c.id";
            whereString += " AND c.slug = '" + category + "'";
        }

        if (tags != null && tags.size() > 0) {
            sqlString += " LEFT OUTER JOIN " + tablePrefix + "photo_tag_rel AS rel ON p.id = rel.photo_id " +
                "LEFT OUTER JOIN " + tablePrefix + "phototag AS tag ON rel.tag_id = tag.id";
            if (tags.size() == 1) {
                whereString += " AND tag.slug = '" + tags.get(0) + "'";
            }
            else {
                whereString += " AND tag.slug IN (" + tags.stream().map(t -> "'" + t + "'").collect(Collectors.joining(", ")) + ")";
            }
        }

        sqlString += whereString + " ORDER BY p.photo_date DESC, p.title";

        logger.info("Created search query: {}", sqlString);

        EntityManager session = entityManagerFactory.createEntityManager();
        try {
            //noinspection unchecked
            List<Object[]> results = session.createNativeQuery(sqlString).getResultList();
            return results.stream()
                .map(row -> PhotoViewModel.builder()
                    .uuid((String) row[1])
                    .title((String) row[2])
                    .path((String) row[3])
                    .approved(row[4] == null ? false : (Boolean) row[4])
                    .photoDate((Date) row[5])
                    .photographer((String) row[6])
                    .tags(getTags(((BigInteger)row[0]).longValue()))
                    .build())
                .collect(Collectors.toList());
        }
        catch (NoResultException e){
            return new ArrayList<>();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }
}
