package se.opkoko.ImageServer.repository;

import org.springframework.data.repository.Repository;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoCategory;

import java.util.List;


public interface CategoryRepository extends Repository<PhotoCategory, Long> {
    PhotoCategory findById(Long id);
    List<PhotoCategory> findAllByOrderByNameAsc();
    PhotoCategory findBySlug(String slug);
}
