package se.opkoko.ImageServer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.opkoko.ImageServer.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
