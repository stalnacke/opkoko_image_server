package se.opkoko.ImageServer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoTag;
import se.opkoko.ImageServer.domain.Role;

public interface TagRepository extends JpaRepository<PhotoTag, Long> {
    PhotoTag findBySlug(String slug);
}
