package se.opkoko.ImageServer.repository.creditcard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import se.opkoko.ImageServer.domain.CreditCard;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;


@Component
public class CreditCardEncryptor {
    private static final Logger logger = LoggerFactory.getLogger(CreditCardEncryptor.class);

    @Value("${images.creditcard.password:1234567812345687}")
    private String key;

    private static final String CRYPTO_SPECS = "AES/CBC/PKCS5Padding";
    private static final int IV_SIZE = 16;

    protected CreditCard unmarshal(String creditCardString) throws ParserConfigurationException, SAXException, IOException {
        CreditCardHandler creditCardHandler = new CreditCardHandler();
        CreditCard creditCard = null;
        if (creditCardString != null) {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new InputSource(new StringReader(creditCardString)), creditCardHandler);
            creditCard = creditCardHandler.getCreditCard();
        }
        return creditCard;
    }

    protected String marshal(CreditCard creditCard) throws JAXBException {
        StringBuilder sb = new StringBuilder();

        sb.append("<creditCard>");
        sb.append(buildXMLTag("cardHolder",creditCard.getCardHolder()));
        sb.append(buildXMLTag("cvv",creditCard.getCvv()));
        sb.append(buildXMLTag("exp",creditCard.getExp()));
        sb.append(buildXMLTag("number",creditCard.getNumber()));
        sb.append(buildXMLTag("type",creditCard.getType().name()));
        sb.append("</creditCard>");

        return sb.toString();
    }

    CreditCard decryptCreditCard(String encryptedCreditCard) {
        logger.debug("Decrypting credit card");

        CreditCard creditCard = null;
        try {
            creditCard = unmarshal(decrypt(encryptedCreditCard));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.info("Error parsing xml");
        }

        return creditCard;
    }

    String encryptCreditCard(CreditCard creditCard) {
        logger.info("Encrypting credit card for " + creditCard.getCardHolder());

        String encryptedCreditCard = null;
        try {
            encryptedCreditCard = encrypt(marshal(creditCard));
        } catch (JAXBException e) {
            logger.info("Error converting to xml");
        }
        return encryptedCreditCard;
    }

    private String encrypt(String creditCardString) {
        String base64EncryptedCreaditCardString = null;

        try {
            // Set IV
            final byte[] iv = new byte[IV_SIZE];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);

            // Encrypt
            Cipher cipher = Cipher.getInstance(CRYPTO_SPECS);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(), new IvParameterSpec(iv));
            byte[] encryptedCreditCardBytes = cipher.doFinal(creditCardString.getBytes(StandardCharsets.UTF_8));

            // Add IV to encrypted creditcard
            byte[] encryptedCreditCardBytesWithIV = new byte[IV_SIZE + encryptedCreditCardBytes.length];
            System.arraycopy(iv, 0, encryptedCreditCardBytesWithIV, 0, IV_SIZE);
            System.arraycopy(encryptedCreditCardBytes, 0, encryptedCreditCardBytesWithIV, IV_SIZE, encryptedCreditCardBytes.length);

            // base 64 encode encryption
            base64EncryptedCreaditCardString = Base64.getEncoder().encodeToString(encryptedCreditCardBytesWithIV);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            logger.info("Failed to encrypt");
        }

        return base64EncryptedCreaditCardString;
    }

    private String decrypt(String encryptedCreditCard) {
        String creditCardString = null;

        // base 64 decode encryption
        byte[] encryptedCreditCardBytesWithIV = Base64.getDecoder().decode(encryptedCreditCard);

        try {
            // Set IV
            final byte[] iv = new byte[IV_SIZE];
            System.arraycopy(encryptedCreditCardBytesWithIV, 0, iv, 0, IV_SIZE);

            // Remove IV from encrypted credit card.
            int encryptedSize = encryptedCreditCardBytesWithIV.length - IV_SIZE;
            byte[] encryptedCreditCardBytes = new byte[encryptedSize];
            System.arraycopy(encryptedCreditCardBytesWithIV, IV_SIZE, encryptedCreditCardBytes, 0, encryptedSize);

            // Decrypt
            Cipher cipher = Cipher.getInstance(CRYPTO_SPECS);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(), new IvParameterSpec(iv));
            byte[] creditCardBytes = cipher.doFinal(encryptedCreditCardBytes);
            creditCardString = new String(creditCardBytes);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | InvalidAlgorithmParameterException  e) {
            logger.info("Failed to decrypt");
        }

        return creditCardString;
    }

    private SecretKey getSecretKey(){
        return new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
    }

    private String buildXMLTag(String tag, String value){
        StringBuilder sb = new StringBuilder();

        sb.append("<").append(tag).append(">");
        sb.append(value);
        sb.append("</").append(tag).append(">");

        return sb.toString();
    }
}
