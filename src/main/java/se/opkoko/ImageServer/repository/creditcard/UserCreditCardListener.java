package se.opkoko.ImageServer.repository.creditcard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.opkoko.ImageServer.domain.User;

import javax.persistence.PostLoad;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Component
public class UserCreditCardListener {
    public static final int MINIMUM_VALIABLE_CREDITCARD_LENGTH = 100;
    static private CreditCardEncryptor encryptor;

    private static final Logger logger = LoggerFactory.getLogger(UserCreditCardListener.class);

    /**
     * Decrypt credit card after loading.
     */
    @PostLoad
    @PostUpdate
    public void decryptCreditCard(Object pc) {
        if (!(pc instanceof User)) {
            return;
        }

        User user = (User) pc;
        user.setCreditCard(null);

        logger.debug("Decrypt " + pc + " with encryptor " + encryptor);

        if (user.getEncryptedCreditCard() != null &&
            user.getEncryptedCreditCard().length() > MINIMUM_VALIABLE_CREDITCARD_LENGTH) {
            user.setCreditCard(encryptor.decryptCreditCard(user.getEncryptedCreditCard()));
        }
    }

    /**
     * Encrypt credit card before persisting
     */
    @PrePersist
    @PreUpdate
    public void encryptCreditCard(Object pc)  {
        if (!(pc instanceof User)) {
            return;
        }

        User user = (User) pc;
        user.setEncryptedCreditCard(null);

        if (user.getCreditCard() != null) {
            user.setEncryptedCreditCard(encryptor.encryptCreditCard(user.getCreditCard()));
        }
    }

    @Autowired
    public void init(CreditCardEncryptor encryptor)
    {
        UserCreditCardListener.encryptor = encryptor;
    }
}
