package se.opkoko.ImageServer.repository.creditcard;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import se.opkoko.ImageServer.domain.CreditCard;
import se.opkoko.ImageServer.domain.CreditCardType;

public class CreditCardHandler extends DefaultHandler {
    private String currentElement;
    private CreditCard.CreditCardBuilder creditCardBuilder;

    public CreditCard getCreditCard() {
        return creditCardBuilder.build();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        if ("creditCard".equals(qName)) {
            creditCardBuilder = CreditCard.builder();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentElement = null;
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if ("type".equals(currentElement)) {
            creditCardBuilder.type(CreditCardType.fromString(new String(ch, start, length)));
        }
        if ("cardHolder".equals(currentElement)) {
            creditCardBuilder.cardHolder(new String(ch, start, length));
        }
        if ("number".equals(currentElement)) {
            creditCardBuilder.number(new String(ch, start, length));
        }
        if ("cvv".equals(currentElement)) {
            creditCardBuilder.cvv(new String(ch, start, length));
        }
        if ("exp".equals(currentElement)) {
            creditCardBuilder.exp(new String(ch, start, length));
        }
    }
}
