package se.opkoko.ImageServer.honeypot;

import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.stream.Collectors;

/**
 * Create a semi-unique fingerprint for a browser
 */
public class Fingerprinter {

    String checksumHeaders[] = {
        "User-Agent",
        "Accept-Encoding",
        "Accept-Language",
        "Cache-Control",
        "DNT",
        "Pragma"
    };

    public String createFingerprint(HttpServletRequest request) {
        String fingerprint = Arrays.stream(checksumHeaders).map(request::getHeader).map(value -> value == null ? "-" : value).collect(Collectors.joining(":"));
        return DigestUtils.md5DigestAsHex(fingerprint.getBytes());
    }

}
