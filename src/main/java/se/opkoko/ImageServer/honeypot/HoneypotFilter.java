package se.opkoko.ImageServer.honeypot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import se.opkoko.ImageServer.honeypot.Fingerprinter;
import se.opkoko.ImageServer.repository.HoneypotRepository;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
public class HoneypotFilter implements Filter {

    private final HoneypotRepository honeypotRepository;
    private final Fingerprinter fingerprinter;


    @Value("${images.table_prefix:}")
    private String tablePrefix;

    @Autowired
    public HoneypotFilter(HoneypotRepository honeypotRepository) {
        this.honeypotRepository = honeypotRepository;
        fingerprinter = new Fingerprinter();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private boolean shouldRun(HttpServletRequest request) {
        String pathInfo = request.getServletPath();
        return pathInfo != null && !(pathInfo.startsWith("/honeypot") || pathInfo.startsWith("/static"));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean honeypotRedirect = false;
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            boolean hasHoneypotCookie = hasHoneypotCookie(httpRequest);
            if (shouldRun(httpRequest) && (hasHoneypotCookie || honeypotRepository.isBlocked(fingerprinter.createFingerprint(httpRequest), getClientIp(httpRequest)))) {
                honeypotRedirect = true;
                ((HttpServletResponse)response).sendRedirect("/honeypot.blocked");
            }
        }

        if (!honeypotRedirect) {
            chain.doFilter(request, response);
        }
    }

    private boolean hasHoneypotCookie(HttpServletRequest httpRequest) {
        Cookie[] cookies = httpRequest.getCookies();
        return cookies != null && Arrays.stream(cookies).anyMatch(c -> c.getName().equals("honeypot"));
    }

    private String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }



    @Override
    public void destroy() {

    }
}
