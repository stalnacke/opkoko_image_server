package se.opkoko.ImageServer.forms;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@ToString
public class LoginForm {

    @NotNull
    @Size(min=2, max=30)
    @Getter
    @Setter
    private String username;

    @NotNull
    @Size(min=2, max=30)
    @Getter
    @Setter
    private String password;

}
