package se.opkoko.ImageServer.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="picture")
@ToString
@Builder
@Getter
@Setter
public class PhotoUpload {
    private String title;
    private String body;
    private int width;
    private int height;
}
