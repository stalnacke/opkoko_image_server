package se.opkoko.ImageServer.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "photo")
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Size(min=32, max = 32)
    private String uuid;

    @NotBlank
    @Size(min=2, max = 100)
    private String title;

    private Boolean approved;

    @NotBlank
    @Size(max = 200)
    private String path;

    private String photographer;
    private String manufacturer;
    private String model;

    private Date photoDate;

    private int width;

    private int height;

    @ManyToMany
    @JoinTable(name = "photo_tag_rel", joinColumns = @JoinColumn(name = "photo_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<PhotoTag> tags;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private PhotoCategory category;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    public void addTag(PhotoTag tag) {
        tags.add(tag);
        tag.getPhotos().add(this);
    }
}
