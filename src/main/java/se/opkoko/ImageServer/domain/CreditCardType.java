package se.opkoko.ImageServer.domain;

public enum CreditCardType {
    VISA,
    MASTERCARD,
    AMERICAN_EXPRESS;

    public static CreditCardType fromString(String s) {
        for (CreditCardType type : CreditCardType.values()) {
            if (type.toString().replaceAll("_", "").equalsIgnoreCase(s.replaceAll("_", ""))) {
                return type;
            }
        }
        return null;
    }
}
