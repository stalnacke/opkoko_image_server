package se.opkoko.ImageServer.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
@XmlRootElement
public class CreditCard {
    private CreditCardType type;
    private String cardHolder;
    private String number;
    private String cvv;
    private String exp;

    public String getDisplayNumber(){
        String mask = "******";
        int lastPart = 4;

        return  number.substring(0,number.length()-lastPart-mask.length())+
                mask+
                number.substring(number.length()-4,number.length());
    }
}
