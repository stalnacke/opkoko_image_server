package se.opkoko.ImageServer.domain.autotagger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@Builder
@Embeddable
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutoTaggerTag {
    String name;
    String slug;
}
