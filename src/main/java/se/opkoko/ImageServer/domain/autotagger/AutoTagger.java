package se.opkoko.ImageServer.domain.autotagger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.util.List;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutoTagger {
    private List<AutoTaggerTag> tags;
}
