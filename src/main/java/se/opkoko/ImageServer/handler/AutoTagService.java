package se.opkoko.ImageServer.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import se.opkoko.ImageServer.domain.autotagger.AutoTagger;
import se.opkoko.ImageServer.domain.Photo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Component
public class AutoTagService {
    @Value("${images.autotager_auth_key}")
    public String tagAuth;
    @Value("${images.tag_backend:https://ctf.opkoko.se/autotagger/}")
    public String tagBackend;

    @Value("${images.photo_directory:.}")
    public String photoDirectory;

    private static Logger logger = LoggerFactory.getLogger(AutoTagService.class);

    AutoTagger convertStreamIntoAutoTagger(InputStream inputStream) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<AutoTagger> typeReference = new TypeReference<AutoTagger>(){};
        return mapper.readValue(inputStream, typeReference);
    }

    private byte[] getPhotoData(Photo photo) throws IOException {
        Path path = Paths.get(photoDirectory, photo.getPath(), photo.getUuid() + "_large.jpg");
        return Files.readAllBytes(path);
    }

    public Optional<AutoTagger> getTagsForPhoto(Photo photo) {
        InputStream inputStream;
        try {
            URL url = new URL(tagBackend);

            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(1000);
            httpCon.setReadTimeout(1000);
            httpCon.setDoOutput(true);
            httpCon.setRequestProperty("User-Agent", "OpKoKo CTF/1.0");
            httpCon.setRequestProperty("X-AUTOTAGGER-AUTH", tagAuth);
            OutputStream outputStream = httpCon.getOutputStream();
            outputStream.write(getPhotoData(photo));
            inputStream = httpCon.getInputStream();

            return Optional.of(convertStreamIntoAutoTagger(inputStream));
        } catch (IOException e) {
            logger.info("Failed to get tags from autotaggning service.", e);
            return Optional.empty();
        }

    }

}
