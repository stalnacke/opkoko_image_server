package se.opkoko.ImageServer.handler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import se.opkoko.ImageServer.domain.PhotoUpload;

public class PhotoUploadHandler extends DefaultHandler {
    private String currentElement;
    private PhotoUpload.PhotoUploadBuilder photoUploadBuilder;

    public PhotoUpload getUploadedPicture() {
        return photoUploadBuilder.build();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = qName;
        if ("picture".equals(qName)) {
            photoUploadBuilder = PhotoUpload.builder();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentElement = null;
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if ("title".equals(currentElement)) {
            photoUploadBuilder.title(new String(ch, start, length));
        }
        if ("body".equals(currentElement)) {
            photoUploadBuilder.body(new String(ch, start, length));
        }
        if ("width".equals(currentElement)) {
            photoUploadBuilder.width(new Integer(new String(ch, start, length)));
        }
        if ("height".equals(currentElement)) {
            photoUploadBuilder.height(new Integer(new String(ch, start, length)));
        }
    }
}
