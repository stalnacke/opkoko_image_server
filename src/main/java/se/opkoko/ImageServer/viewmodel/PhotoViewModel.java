package se.opkoko.ImageServer.viewmodel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoTag;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Getter
@Builder
@AllArgsConstructor
public class PhotoViewModel {
    private final String uuid;
    private final String title;
    private final String path;
    private final Boolean approved;
    private final Date photoDate;
    private final String photographer;
    private final List<PhotoTag> tags;
    private final Map<String, Object> exifInformation;

    public PhotoViewModel(Photo photo) {
        this.uuid = photo.getUuid();
        this.title = photo.getTitle();
        this.approved = photo.getApproved();
        this.photoDate = photo.getPhotoDate();
        this.photographer = photo.getPhotographer();
        this.path = photo.getPath();
        Set<PhotoTag> tagSet = photo.getTags();
        if (tagSet != null) {
            this.tags = new ArrayList<>(tagSet);
        }
        else {
            this.tags = Collections.emptyList();
        }

        this.exifInformation = new HashMap<>();
        this.exifInformation.put("Fotograf", photo.getPhotographer());
        this.exifInformation.put("Tillverkare", photo.getManufacturer());
        this.exifInformation.put("Kamera", photo.getModel());
        this.exifInformation.put("Bredd", photo.getWidth());
        this.exifInformation.put("Höjd", photo.getHeight());
        this.exifInformation.put("Datum", photo.getPhotoDate() == null ? null : new SimpleDateFormat("yyyy-MM-dd").format(photo.getPhotoDate()));
    }

    public String getDetailsUrl() {
        return "/photos/" + uuid;
    }

    public String getThumbnailUrl() {
        return "/content/" + path + "/" + uuid + "_thumb.jpg";
    }

    public String getLargeUrl() {
        return "/content/" + path + "/" + uuid + "_large.jpg";
    }

    public String getPhotoDateString() {
        if (photoDate == null) {
            return "date unknown";
        }
        return TimeAgo.toRelative(LocalDateTime.ofInstant(photoDate.toInstant(), ZoneId.systemDefault()));
    }
}
