package se.opkoko.ImageServer.viewmodel;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TimeAgo {
    public static String toRelative(LocalDateTime dateTime) {
        return TimeAgo.toRelative(LocalDateTime.now(), dateTime);
    }

    protected static String toRelative(LocalDateTime now, LocalDateTime dateTime) {
        if (now.isBefore(dateTime)) {
            return "i framtiden";
        }
        StringBuilder sb = new StringBuilder();

        long month = ChronoUnit.MONTHS.between(dateTime, now);
        long years = ChronoUnit.YEARS.between(dateTime, now);
        if (years > 0) {
            if (month % 12 <= 3) {
                sb.append("för ca ");
            } else if (month % 12 >= 8){
                sb.append("nästan ");
                years++;
            } else {
                sb.append("drygt ");
            }
            if (years == 1) {
                sb.append("ett år sedan");
            } else {
                sb.append(years + " år sedan");
            }

            return sb.toString();
        }

        if (month > 0 && month < 12) {
            if (month == 1) {
                return "en månad sedan";
            } else {
                return month + " månader sedan";
            }
        }

        long days = ChronoUnit.DAYS.between(dateTime, now);
        if (days > 0) {
            if (days == 1) {
                return "för en dag sedan";
            }
            return days + " dagar sedan";
        }

        long hours = ChronoUnit.HOURS.between(dateTime, now);
        if (hours > 0) {
            if (hours == 1) {
                return "en timme sedan";
            }
            return hours + " timmar sedan";
        }
        return "nyss";
    }
}
