package se.opkoko.ImageServer.viewmodel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
public class AutoTags {

    @Singular
    private List<Tag> newlyAddedTags;
    @Singular
    private List<Tag> unknownTags;


    @AllArgsConstructor
    @Getter
    @ToString
    public static class Tag {
        String name;
        String slug;
    }
}
