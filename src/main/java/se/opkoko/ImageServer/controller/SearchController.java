package se.opkoko.ImageServer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import se.opkoko.ImageServer.domain.PhotoCategory;
import se.opkoko.ImageServer.domain.User;
import se.opkoko.ImageServer.repository.CategoryRepository;
import se.opkoko.ImageServer.repository.PhotoRepository;
import se.opkoko.ImageServer.repository.SearchRepository;
import se.opkoko.ImageServer.repository.UserRepository;
import se.opkoko.ImageServer.viewmodel.PhotoViewModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/search")
public class SearchController {

    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final SearchRepository searchRepository;
    private final PhotoRepository photoRepository;

    private static Logger logger = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    public SearchController(CategoryRepository categoryRepository, UserRepository userRepository, SearchRepository searchRepository, PhotoRepository photoRepository) {
        this.userRepository = userRepository;
        this.searchRepository = searchRepository;
        this.categoryRepository = categoryRepository;
        this.photoRepository = photoRepository;
    }

    @RequestMapping("")
    public String photos(HttpServletRequest request, HttpServletResponse response, Model model) {
        String categoryString = request.getParameter("category");
        PhotoCategory photoCategory = categoryRepository.findBySlug(categoryString);
        model.addAttribute("categories", categoryRepository.findAllByOrderByNameAsc());
        model.addAttribute("category", photoCategory);

        String[] tagsParameter = request.getParameterValues("tag");
        List<PhotoViewModel> photos = new ArrayList<>(searchRepository.getPhotos(categoryString, tagsParameter != null ? Arrays.asList(tagsParameter) : null));
        model.addAttribute("photos", photos);
        if (photoCategory == null) {
            model.addAttribute("tags", searchRepository.getAllTags());
            model.addAttribute("pagename", "allphotos");
        }
        else {
            model.addAttribute("tags", searchRepository.getAllTagsForCategory(photoCategory));
            model.addAttribute("pagename", "search");
        }
        response.setHeader("Cache-Control", "max-age=60");
        response.setHeader("Pragma", "cache");
        return "photos";
    }

    @RequestMapping("myPhotos")
    public String myPhotos(HttpServletRequest request, Model model, Principal principal) {
        User user = userRepository.findByUsername(principal.getName());

        model.addAttribute("categories", categoryRepository.findAllByOrderByNameAsc());
        model.addAttribute("pagename", "myphotos");

        List<PhotoViewModel> photos = photoRepository.findByOwner(user).stream().map(PhotoViewModel::new).collect(Collectors.toList());
        model.addAttribute("photos", photos);

        return "photos";
    }

}
