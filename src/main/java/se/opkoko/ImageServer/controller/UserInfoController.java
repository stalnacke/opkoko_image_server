package se.opkoko.ImageServer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import se.opkoko.ImageServer.domain.CreditCard;
import se.opkoko.ImageServer.domain.User;
import se.opkoko.ImageServer.repository.UserRepository;
import se.opkoko.ImageServer.repository.creditcard.CreditCardEncryptor;
import se.opkoko.ImageServer.repository.creditcard.UserCreditCardListener;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/user_info")
public class UserInfoController extends WebMvcConfigurerAdapter {
    private final UserRepository userRepository;
    private final UserCreditCardListener userCreditCardListener;

    @Autowired
    public UserInfoController(UserRepository userRepository, UserCreditCardListener userCreditCardListener) {
        this.userRepository = userRepository;
        this.userCreditCardListener = userCreditCardListener;
    }

    @RequestMapping("")
    public String info(Model model, Authentication authentication) {
        User user = userRepository.findByUsername(((UserDetails) authentication.getPrincipal()).getUsername());
        model.addAttribute("userinfo", user);

        userCreditCardListener.decryptCreditCard(user);
        CreditCard creditcard = user.getCreditCard();
        if (creditcard != null) {
            model.addAttribute("creditcard", creditcard);
        }
        return "user_info";
    }
}
