package se.opkoko.ImageServer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import se.opkoko.ImageServer.honeypot.Fingerprinter;
import se.opkoko.ImageServer.repository.HoneypotRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;

/*
 * In computer terminology, a honeypot is a computer security mechanism set to detect, deflect, or, in some manner,
 * counteract attempts at unauthorized use of information systems. Generally, a honeypot consists of data
 * (for example, in a network site) that appears to be a legitimate part of the site, but is actually isolated and
 * monitored, and that seems to contain information or a resource of value to attackers, who are then blocked.
 * This is similar to police sting operations, colloquially known as "baiting," a suspect.
 *  - https://en.wikipedia.org/wiki/Honeypot_%28computing%29
 *
 * This honeypot works by embedding a link to this page in the framework of every page.
 * The link clearly stats that the link should not be followed and we have added the url
 * to the robots asking them not to index the page. Browsers that follows the link anyway
 * should be blocked.
 */
@Controller
public class HoneypotController {

    private final HoneypotRepository honeypotRepository;

    @Autowired
    public HoneypotController(HoneypotRepository honeypotRepository) {
        this.honeypotRepository = honeypotRepository;
    }

    private String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }

    @RequestMapping("/honeypot")
    public String honeypot(HttpServletRequest request, HttpServletResponse response) {
        Fingerprinter fingerprinter = new Fingerprinter();

        String fingerprint = fingerprinter.createFingerprint(request);
        String ip = getClientIp(request);
        boolean blockedBrowser = honeypotRepository.isBlocked(fingerprint, ip);
        if (!blockedBrowser) {
            honeypotRepository.add(fingerprint, ip);
        }

        Cookie honeypotCookie = new Cookie("honeypot", "WW91IHNob3VsZCBub3QgdmlzaXQgdGhpcyBwYWdlLiBJdCdzIGEgaG9uZXlwb3Q=");
        honeypotCookie.setHttpOnly(true);
        honeypotCookie.setMaxAge(600);
        //response.addCookie(honeypotCookie);
        return "redirect:honeypot.blocked";
    }

    @RequestMapping("/honeypot.blocked")
    public String honeypotBlocked(HttpServletResponse response) {
        return "blocked";
    }

    /**
     * Cleanup blocked entries in the database database
     */
    @Scheduled(fixedRate = 30_000, initialDelay = 60_000)
    public void reportCurrentTime() {
        honeypotRepository.cleanup();
    }
}
