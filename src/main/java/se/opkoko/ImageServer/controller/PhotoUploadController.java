package se.opkoko.ImageServer.controller;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoUpload;
import se.opkoko.ImageServer.domain.User;
import se.opkoko.ImageServer.handler.PhotoUploadHandler;
import se.opkoko.ImageServer.repository.PhotoRepository;
import se.opkoko.ImageServer.repository.UserRepository;
import se.opkoko.ImageServer.viewmodel.UploadedPhotoResponse;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;
import java.util.UUID;

@RestController
public class PhotoUploadController {

    private static Logger logger = LoggerFactory.getLogger(PhotoUploadController.class);

    private final PhotoRepository photoRepository;
    private final UserRepository userRepository;

    @Value("${images.upload_directory:.}")
    public String uploadDirectory;

    private static final String IMAGE_MARKER = "data:image/jpeg;base64,";

    @Autowired
    public PhotoUploadController(PhotoRepository photoRepository, UserRepository userRepository) {
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
    }

    private String getUuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "").toUpperCase();
    }

    private String getAndCreateBaseDirectory(String uuid) {
        String pathname = uploadDirectory + "/" + uuid.substring(0, 2) + "/" + uuid.substring(2, 4);
        File dir = new File(pathname);
        if (!dir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            dir.mkdirs();
        }
        return pathname;
    }

    private String getFilePath(String pathname, String uuid, String sufix) {
        return pathname + "/" + uuid + "_" + sufix + ".jpg";
    }

    private void validateImage(PhotoUpload picture) {
        if (picture == null || picture.getBody() == null) {
            throw new IllegalArgumentException("Image file seems broken");
        }
        if (!picture.getBody().startsWith(IMAGE_MARKER)) {
            throw new IllegalArgumentException("Image file seems broken");
        }
    }

    @PostMapping(value = "/upload")
    public UploadedPhotoResponse postCustomer(HttpServletRequest request, Principal principal){
        PhotoUploadHandler photoUploadHandler = new PhotoUploadHandler();

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            InputSource is = new InputSource(request.getInputStream());
            is.setEncoding("UTF-8");
            saxParser.parse(is, photoUploadHandler);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        PhotoUpload photoUpload = photoUploadHandler.getUploadedPicture();
        validateImage(photoUpload);
        String uuid = getUuid();
        String baseDirectory = getAndCreateBaseDirectory(uuid);

        User user = userRepository.findByUsername(principal.getName());

        byte[] largePhotoData = Base64.decodeBase64(photoUpload.getBody().substring(IMAGE_MARKER.length()));

        byte[] thumbnailPhotoData = resize(largePhotoData,400,400);
        try{
            OutputStream stream = new FileOutputStream(getFilePath(baseDirectory, uuid, "large"));
            stream.write(largePhotoData);

            stream = new FileOutputStream(getFilePath(baseDirectory, uuid, "thumb"));
            stream.write(thumbnailPhotoData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Photo photo = Photo.builder()
            .uuid(uuid)
            .path(uuid.substring(0, 2) + "/" + uuid.substring(2, 4))
            .title(photoUpload.getTitle().split("\\.")[0])
            .approved(false)
            .owner(user)
            .photoDate(new Date())
            .height(photoUpload.getHeight())
            .width(photoUpload.getWidth())
            .manufacturer("opkoko")
            .model("2018.1")
            .photographer(user.getFirstName())
            .build();
        photoRepository.save(photo);
        logger.info("A new photo with uuid {} was uploaded by {}", uuid, principal.getName());

        return UploadedPhotoResponse.builder().uuid(uuid).build();
    }


    private static byte[] resize(byte[] img, int height, int width) {
        byte[] bytes = null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(img);
            BufferedImage image = ImageIO.read(bais);
            int srcWidth=image.getWidth();
            int srcHeight = image.getHeight();
            double y = 0;
            double x = 0;
            if (srcWidth > width) {
                x = (srcWidth-width) / 2;
            }
            if (srcHeight > height){
                y = (srcHeight-height) / 2;
            }

            Rectangle rect = new Rectangle((int)x,(int)y,width,height);
            BufferedImage cropped = image.getSubimage(rect.x, rect.y, rect.width, rect.height);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(cropped, "jpg", baos);
            bytes = baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bytes;
    }
}
