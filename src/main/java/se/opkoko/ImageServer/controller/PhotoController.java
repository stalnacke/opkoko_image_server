package se.opkoko.ImageServer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import se.opkoko.ImageServer.domain.Photo;
import se.opkoko.ImageServer.domain.PhotoTag;
import se.opkoko.ImageServer.domain.User;
import se.opkoko.ImageServer.domain.autotagger.AutoTagger;
import se.opkoko.ImageServer.handler.AutoTagService;
import se.opkoko.ImageServer.repository.CategoryRepository;
import se.opkoko.ImageServer.repository.PhotoRepository;
import se.opkoko.ImageServer.repository.SearchRepository;
import se.opkoko.ImageServer.repository.TagRepository;
import se.opkoko.ImageServer.repository.UserRepository;
import se.opkoko.ImageServer.viewmodel.AutoTags;
import se.opkoko.ImageServer.viewmodel.PhotoViewModel;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/photos")
public class PhotoController {

    private final CategoryRepository categoryRepository;
    private final PhotoRepository photoRepository;
    private final TagRepository tagRepository;
    private final SearchRepository searchRepository;
    private final UserRepository userRepository;

    private final AutoTagService autoTagService;

    private static Logger logger = LoggerFactory.getLogger(PhotoController.class);

    @Autowired
    public PhotoController(CategoryRepository categoryRepository,
                           PhotoRepository photoRepository,
                           TagRepository tagRepository,
                           SearchRepository searchRepository, UserRepository userRepository, AutoTagService autoTagService) {
        this.categoryRepository = categoryRepository;
        this.photoRepository = photoRepository;
        this.tagRepository = tagRepository;
        this.searchRepository = searchRepository;
        this.userRepository = userRepository;
        this.autoTagService = autoTagService;
    }

    @RequestMapping("")
    public String photos(Model model) {
        model.addAttribute("categories", categoryRepository.findAllByOrderByNameAsc());
        List<PhotoViewModel> photos = photoRepository.findAll()
            .stream()
            .map(PhotoViewModel::new)
            .collect(Collectors.toList());
        model.addAttribute("photos", photos);
        model.addAttribute("tags", searchRepository.getAllTags());

        return "photos";
    }

    @RequestMapping(value = "/{photoUuid:[a-zA-Z0-9]{32}}", method = {RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS})
    public String photo(HttpServletRequest request, Model model, @PathVariable String photoUuid, Principal principal) {
        Photo photo = photoRepository.findByUuid(photoUuid);
        User user = userRepository.findByUsername(principal.getName());
        if (!photo.getApproved() && photo.getOwner() != user) {
            throw new DraftPhotoException("Bilder som ännu inte är godkända får endast visas av deras ägare.");
        }
        model.addAttribute("categories", categoryRepository.findAllByOrderByNameAsc());
        List<PhotoTag> unusedTags = searchRepository.getAllTags().stream()
            .filter(i -> !photo.getTags().contains(i))
            .collect(Collectors.toList());
        model.addAttribute("unusedTags", unusedTags);
        model.addAttribute("photo_tags", photo.getTags().toString());
        model.addAttribute("photo", new PhotoViewModel(photo));
        model.addAttribute("category", photo.getCategory());

        return "photo_details";
    }

    @RequestMapping(value = "/{photoUuid:[a-zA-Z0-9]{32}}/autotags")
    @ResponseBody
    public AutoTags autoTags(@PathVariable String photoUuid) {
        Photo photo = photoRepository.findByUuid(photoUuid);
        Optional<AutoTagger> tagsForPhoto = autoTagService.getTagsForPhoto(photo);

        List<String> currentSlugs = photo.getTags().stream().map(PhotoTag::getSlug).collect(Collectors.toList());

        Map<Boolean, List<AutoTags.Tag>> newAndExistingTags = tagsForPhoto.map(foundTags -> foundTags.getTags()
            .stream()
            .map(tag -> new AutoTags.Tag(tag.getName(), tag.getSlug()))
            .collect(Collectors.partitioningBy(tag -> currentSlugs.contains(tag.getSlug()))))
            .orElse(Collections.emptyMap());

        AutoTags.AutoTagsBuilder builder = AutoTags.builder();

        if (!newAndExistingTags.isEmpty()) {
            newAndExistingTags.get(false)
                .forEach(tag -> {
                    PhotoTag photoTag = tagRepository.findBySlug(tag.getSlug());
                    if (photoTag == null) {
                        builder.unknownTag(tag);
                        /*photoTag = PhotoTag.builder()
                            .name(tag.getName())
                            .slug(tag.getSlug())
                            .photos(new HashSet<>())
                            .build();
                        tagRepository.save(photoTag);*/
                    }
                    else {
                        photo.addTag(photoTag);
                        photoRepository.save(photo);
                        builder.newlyAddedTag(tag);
                    }
                });
        }

        return builder.build()
;    }

    @RequestMapping(value = "/{photoUuid:[a-zA-Z0-9]{32}}/tag", method = {RequestMethod.POST})
    public String photoAddTag(HttpServletRequest request,
                              Model model,
                              @PathVariable String photoUuid,
                              @RequestParam String tag) {
        Photo photo = photoRepository.findByUuid(photoUuid);


        PhotoTag photoTag = tagRepository.findBySlug(tag);
        if (request.getParameter("delete") !=  null) {
            photo.getTags().remove(photoTag);
        }
        else {
            photo.getTags().add(photoTag);
        }
        photoRepository.save(photo);

        return "redirect:/photos/" + photoUuid;
    }

    @RequestMapping("/new")
    public String photoUpload(Model model) {
        model.addAttribute("categories", categoryRepository.findAllByOrderByNameAsc());

        return "photo_new";
    }
}
