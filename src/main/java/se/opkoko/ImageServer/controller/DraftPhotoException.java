package se.opkoko.ImageServer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Bara ägare kan se foton som ännu inte är godkända.")
public class DraftPhotoException extends RuntimeException {
    public DraftPhotoException(String message) {
        super(message);
    }
}
