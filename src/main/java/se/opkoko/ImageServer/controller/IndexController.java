package se.opkoko.ImageServer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/robots.txt")
    @ResponseBody
    public String robots(HttpServletResponse response) {
        response.setContentType("text/plain");
        return "User-agent: *\nDisallow: /\nDisallow: /honeypot";
    }
}
