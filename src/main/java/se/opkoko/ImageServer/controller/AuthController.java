package se.opkoko.ImageServer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import se.opkoko.ImageServer.domain.User;
import se.opkoko.ImageServer.forms.LoginForm;
import se.opkoko.ImageServer.repository.UserRepository;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Controller
public class AuthController extends WebMvcConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private UserRepository users;

    @Autowired
    public AuthController(UserRepository users) {
        this.users = users;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }

    @GetMapping("/login")
    public String showForm(LoginForm loginForm) {
        return "auth/login";
    }

    @PostMapping("/login")
    public String checkLoginInfo(Model model, @Valid LoginForm loginForm, BindingResult bindingResult) throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException {

        logger.error("Inside checkLoginInfo");
        if (bindingResult.hasErrors()) {
            return "auth/login";
        }

        try {
            User user = this.users.findByUsername(loginForm.getUsername());
            if (user == null) {
                model.addAttribute("loginErrorMessage", "User not found or incorrect password");
                return "auth/login";
            }
        }
        catch (Exception e) {
            model.addAttribute("loginErrorMessage", "User not found or incorrect password" + e);
            return "auth/login";

        }

        return "redirect:/index";
    }

    @GetMapping(value="/login", params = "logout")
    public String doLogout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?loggedout";
    }

}
