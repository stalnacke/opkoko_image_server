if (typeof Dropzone !== 'undefined') {

    Dropzone.prototype.defaultOptions.dictDefaultMessage = "Dra och släpp bilder här";
    Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
    Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
    Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.";
    Dropzone.prototype.defaultOptions.dictInvalidFileType = "You can't upload files of this type.";
    Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with {{statusCode}} code.";
    Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancel upload";
    Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Är du säker att du vill avbryta?";
    Dropzone.prototype.defaultOptions.dictRemoveFile = "Remove file";
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";

    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);


    var resizeImage = function (settings) {
        var file = settings.file;
        var maxSize = settings.maxSize;
        var reader = new FileReader();
        var image = new Image();
        var canvas = document.createElement('canvas');
        var dataURItoBlob = function (dataURI) {
            var bytes = dataURI.split(',')[0].indexOf('base64') >= 0 ?
                atob(dataURI.split(',')[1]) :
                unescape(dataURI.split(',')[1]);
            var mime = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var max = bytes.length;
            var ia = new Uint8Array(max);
            for (var i = 0; i < max; i++)
                ia[i] = bytes.charCodeAt(i);
            return new Blob([ia], { type: mime });
        };
        var resize = function () {
            var width = image.width;
            var height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var dataUrl = canvas.toDataURL('image/jpeg');
            return dataURItoBlob(dataUrl);
        };
        return new Promise(function (ok, no) {
            if (!file.type.match(/image.*/)) {
                no(new Error("Not an image"));
                return;
            }
            reader.onload = function (readerEvent) {
                image.onload = function () { return ok(resize()); };
                image.src = readerEvent.target.result;
            };
            reader.readAsDataURL(file);
        });
    };

    Dropzone.options.imageUploadDropzone = {
        paramName: "file",
        maxFilesize: 10, // MB
        createImageThumbnails: true,
        acceptedFiles: 'image/*',
        previewsContainer: '#imageUploadPreview',
        addRemoveLinks: false,
        maxFiles: 1,
        autoQueue: false,
        previewTemplate: previewTemplate,
        thumbnailWidth: 800,
        thumbnailHeight: 800,
        resize: function (file, width, height) {
            var h = Math.min(file.height, this.options.thumbnailHeight),
                w = Math.min(file.width, this.options.thumbnailWidth);
            if (file.width > file.height) {
                h = (file.height / file.width) * w;
            }
            else {
                w = (file.width / file.height) * h;
            }
            return {
                srcX: 0,
                srcY: 0,
                trgX: 0,
                trgY: 0,
                srcWidth: file.width,
                srcHeight: file.height,
                trgWidth: w,
                trgHeight: h
            };
        },
        init: function() {
            var that = this;
            this.on("addedfile", function(file) {
                document.querySelector('#image-upload-dropzone').className += " d-none";
                file.previewElement.querySelector(".start").onclick = function() {
                    resizeImage({
                        file: file,
                        maxSize: 800
                    }).then(function (resizedImage) {
                        var reader = new FileReader();
                        reader.readAsDataURL(resizedImage);
                        reader.onloadend = function() {
                            base64data = reader.result;
                            var xml = "<picture>" +
                                "<title>" + file.name + "</title>" +
                                "<body><![CDATA[" + base64data + "]]></body>" +
                                "<width>" + file.width + "</width>" +
                                "<height>" + file.height + "</height>" +
                                "</picture>";

                            $.ajax({
                                url: "/upload",
                                data: xml,
                                cache: false,
                                contentType: "application/xml;charset=UTF-8",
                                processData: false,
                                type: 'POST',
                                success: function(data){
                                    uuid = data.children[0].children[0].innerHTML;
                                    var newPhotoMessage = $("<div class=\"alert alert-info alert-dismissible fade show\" role=\"alert\">\n" +
                                        "  <strong>Nytt foto!</strong> <a href=\"/photos/" + uuid + "\">Ditt nya foto</a>.\n" +
                                        "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                                        "    <span aria-hidden=\"true\">&times;</span>\n" +
                                        "  </button>\n" +
                                        "</div>");
                                    $('#upload-messages').append(newPhotoMessage);
                                    that.removeAllFiles();
                                    $('#image-upload-dropzone').removeClass('d-none');
                                }
                            });
                        }

                    }).catch(function (err) {
                        console.error(err);
                    });
                };
                file.previewElement.querySelector(".cancel").onclick = function() { $('#image-upload-dropzone').removeClass('d-none') };
            });
            this.on("sending", function(file) {
                file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
            });
        }
    };
}



$(document).ready(function () {
    $('.images-photo-card').on('click', function (event) {
        var element = $(this);
        event.preventDefault();
        window.location.href = element.find('.images-js-click-target').attr('href');
    });

    $('#autotagger').on('click', function (event) {
        var element = $(this);
        event.preventDefault();
        var photoId = element.data('photoid');
        var url = '/photos/' + photoId + "/autotags";
        $.getJSON(url, function (data){
            var message = "";
            var i;
            if (data.newlyAddedTags.length > 0) {
                message += "Nya taggar:\n";
                for (i = 0; i < data.newlyAddedTags.length; i++) {
                    message += data.newlyAddedTags[i].name + "\n"
                }
                message += "\n";
            }
            if (data.unknownTags.length > 0) {
                message += "Okända taggar\n(taggar bara med redan existerande taggar)\n";
                for (i = 0; i < data.unknownTags.length; i++) {
                    message += data.unknownTags[i].name + "\n"
                }
            }
            if (data.newlyAddedTags.length === 0 && data.unknownTags.length === 0) {
                message = "Hittar inga nya taggar. Har vi rättigheter att anropa autotaggningstjänsten?";
            }
            alert(message);
            window.location.reload(true);
        })
    });



});

$('#editForm').click(function(){
    return false;
});
