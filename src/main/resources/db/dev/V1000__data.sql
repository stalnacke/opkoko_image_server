-- noinspection SqlResolveForFile


-- Create some DEV users
-- admin password
-- user  user
insert into user (id, username, first_name, last_name, password, salt, access_token, creditcard) values
    (1, 'admin', 'Admin', 'Administrator', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'B00B005186AA40F0A10614DD0C0EB54A', '7574F42C315A4A0A986D249C5C01B839', null),
    (2, 'user', 'User', 'Developer', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '6E316B6CCA224373B2349CC3347529AB', '6F40013BF8E5436AB3B9D34C87D3C26D', '/hYb6GIIv0WaJRSZaV0TqoSxmOq3E/QEkal+L7kwL68+CMizbswHooeOAqdku4NHLMgK3VSSa2WlkRPr5CO7uLvUxYLsOoJVVHzoEFLjmDP/FUyF0uGOiPlKusZ5rvCSX8RLVTBCY/IzFfBAmkmXB+v1JVebMWYBm4Y5ulB3z7GvQgcwb+VTWsAldzfQ+EqGcIO9pU/7Pm7Yw9X1VMcw+hQLUM1Aj3R+h59zj8ONRqM=');

insert into role (id, name) values
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

insert into user_role_rel (user_id, role_id) values
(1, 1),
(2, 2);

insert into category (id, name, slug) values
    (1, 'Träd',   'trad'),
    (2, 'Blomma', 'blomma'),
    (3, 'Djur', 'djur'),
    (4, 'Naturen', 'Naturen');


-- insert the data
INSERT INTO photo (category_id, title, uuid, path, photographer, width, height, photo_date, owner_id, approved) VALUES
(1, 'Ett stort träd', '7ABCACA975AD4556B47DD3B02DD721DF', '7A/BC', 'Vår Fotograf', 1024, 768,'2018-02-12 12:22:11', 1, true),
(1, 'Ett litet träd', '6E8823676783497EA56E3EC38EE9E38B', '6E/88', 'Jag', 2048, 1356, '2012-02-12 12:22:11', 1, true),
(1, 'Ett roligt träd', 'B74231C92EB14447B03FBF3E9D4AD5F2', 'B7/42', 'Någon annan', 640, 480, '2003-02-12 12:22:11', 1, true),
(1, 'En buske', 'B1F7AF64119047798A9F4E2A6B346BCE', 'B1/F7', '',  640,480 , '2018-02-12 12:11:11', 1, true),
(1, 'En buske utanför huset', '0B22EE2D33074DBD94517556E564F447', '0B/22', '',  640,480, '2012-11-14 15:00:00', 1, true),
(1, 'Ett ensamt träd', '46B1DA5150A44101A46AC0FE9E19BEF7', '46/B1', '',  640,480, '2012-11-14 15:00:00', 1, true),
(1, 'LJulgranen', 'C9CD78BA9FAB4E04B05FDB57A89E20EC', 'C9/CD', '',  640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma001', '2150305DEFEB4A4A864A2B740EFD5701', '21/50', '',  640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma002', 'F735D7AF24264DA2BC0C55DF969C915F', 'F7/35', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma003', '9254ED78AE0443499682465839710464', '92/54', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma004', '92EA939043774ABDB6E5F520B1B0F6EE', '92/EA', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma005', 'E94A2EB0A82F40C6A8A914C86BD4A067', 'E9/4A', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma006', '575EC69A93D243A1BD85074E6BCD46CC', '57/5E', '', 640,480, '2013-01-11 12:22:11', 1, true),
(2, 'Blomma007', '3F0F469E8C5E4257BEA4F9766027F567', '3F/0F', '', 640,480, '2013-01-11 12:22:11', 1, true),
(2, 'Blomma008', 'EBDFF08898B74249B97BDF55A28631CB', 'EB/DF', '', 640,480, '2013-01-11 12:22:11', 2, false),
(2, 'Blomma009', '22BAC988124342778F37D46D0D7B0BF3', '22/BA', '', 640,480, '2013-01-11 12:22:11', 2, false),
(2, 'Blomma010', 'BEA0ACA704F448EF84B02A2DB31A5F5F', 'BE/A0', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma011', 'E0A9E09B9C6145E9ABE845F304A07427', 'E0/A9', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma012', '45C787A56B3A42DE9A91573F3AE4F482', '45/C7', '', 640,480, '2012-11-14 15:00:00', 1, true),
(2, 'Blomma013', 'F0CAB0FC41144E769B7F1CD6F3E85A91', 'F0/CA', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma014', '347C097D0A2C4C3AA1ECBFB72FF0F552', '34/7C', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma015', '2A233808C793456996693EAB0589476E', '2A/23', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma016', 'DBC641B421BE44D9A00A9888B27BCC04', 'DB/C6', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma017', 'BD7AD74715B841CFB0A582CB4FC77680', 'BD/7A', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma018', '55B64BD4792146B1A02180A6BCF74F00', '55/B6', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma019', 'BBD59E41B95D44AA80FCDD3A2362CD6F', 'BB/D5', '', 640,480, '2012-12-24 15:00:01', 1, true),
(2, 'Blomma020', '56CA0CCE22484F9AA5808325C684FEF6', '56/CA', '', 640,480, '2012-12-24 15:00:01', 1, true),
(3, 'Djur01', '046D03084ECB44CEB0756D1DB2D3FE18', '04/6D', '', 640,480, '2012-12-24 15:00:01', 1, true),
(3, 'Djur02', 'E0961FD3D4C44C9E856BF597F761AF55', 'E0/96', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur03', 'E7618F58BCAA4367A91F3E831D4FB06F', 'E7/61', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur04', 'A32DF22C24FF4CC18A59E6EF98289173', 'A3/2D', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur05', 'BEB965E5DAC5499CA06741A66DA8D01D', 'BE/B9', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur06', '687C0108ACE94C66AA676464FE022DBD', '68/7C', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur07', 'B8DA4EE42BD44490881C8D7D74C7D92A', 'B8/DA', '', 640,480, '2013-01-11 12:22:11', 1, true),
(3, 'Djur08', '271F20DCB29140AE9C104823512E61DA', '27/1F', '', 640,480, '2013-01-11 12:22:11', 1, true),
(4, 'Naturen00001', '69A1F0436BCA4DD38E1492D14E5D55EA', '69/A1', '', 640,480, '2013-01-11 12:22:11', 1, true);

-- Create a tag and add it to a photo
INSERT iNTO phototag (id, name, slug) VALUES
    (1, 'Röd', 'rod'),
    (2, 'Grön', 'gron'),
    (3, 'Blå', 'bla'),
    (4, 'Gul', 'gul');

INSERT INTO photo_tag_rel (tag_id, photo_id) VALUES
    (1, 1),
    (1, 10),
    (1, 15),
    (1, 16),
    (1, 18),
    (2, 18),
    (2, 33),
    (3, 2),
    (2, 3),
    (2, 4),
    (2, 5),
    (3, 1),
    (3, 7),
    (3, 10),
    (3, 15),
    (3, 18),
    (3, 36),
    (4, 33);

