
CREATE TABLE user (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) DEFAULT NULL,
    username varchar(100) NOT NULL,
    salt varchar(32) NOT NULL,
    password varchar(100) NOT NULL,
    access_token varchar(50) NOT NULL,
    creditcard varchar(1024) DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_username (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE role (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE user_role_rel (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    user_id bigint(20) NOT NULL,
    role_id bigint(20) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_user_role_rel (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE category (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    slug varchar(50) DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_category_slug (slug)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE photo (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid char(32) NOT NULL,
    category_id bigint(20),
    title varchar(100) NOT NULL,
    path varchar(200) NOT NULL,
    photographer varchar(100) NOT NULL,
    manufacturer varchar(50) NOT NULL,
    model varchar(50) NOT NULL,
    width INTEGER,
    height INTEGER,
    photo_date DATETIME,
    PRIMARY KEY (id),
    CONSTRAINT photo_uuid_idx UNIQUE (uuid),
    FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE phototag (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    slug varchar(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_phototag_slug (slug)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE photo_tag_rel (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    photo_id bigint(20) NOT NULL,
    tag_id bigint(20) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_photo_tag_rel (photo_id, tag_id),
    FOREIGN KEY (photo_id) REFERENCES photo(id) ON DELETE CASCADE,
    FOREIGN KEY (tag_id) REFERENCES phototag(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE honeypot (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    ip varchar(40) NOT NULL,
    blocked_date DATETIME DEFAULT NOW(),
    fingerprint VARCHAR(40),
    PRIMARY KEY (id),
    UNIQUE (ip, blocked_date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
