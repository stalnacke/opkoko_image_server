ALTER TABLE photo ADD COLUMN owner_id bigint(20) DEFAULT 1;
ALTER TABLE photo ADD COLUMN approved boolean DEFAULT false;
UPDATE photo set owner_id = 1;
UPDATE photo set approved = true;
ALTER TABLE photo ADD CONSTRAINT photo_owner_fk FOREIGN KEY (owner_id) REFERENCES user(id) ON DELETE CASCADE;

