FROM openjdk:8-jdk

RUN apt-get update && apt-get install -y cifs-utils sudo && apt-get clean

VOLUME /tmp

ADD "./build/libs/opkoko_image_server-0.0.1-SNAPSHOT.jar" app.jar
ADD "docker/start.sh" /start.sh
ARG IMAGES_SECRET_TOKEN
ENV IMAGES_SECRET_TOKEN="${IMAGES_SECRET_TOKEN}"

RUN useradd --system opkoko_images --uid 999 && \
    echo "${IMAGES_SECRET_TOKEN}" > /etc/secret_image_token && \
    mkdir -p /var/images && \
    echo "//files/ctf	/var/images	cifs	user=root,pass=ctf 0 0" >> /etc/fstab

ENTRYPOINT ["/start.sh"]
