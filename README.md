# Image Server for displaying the images we have for sale.

This app has been created as part of a internal CTF challenge. The code might, or might not
be well written. Please think twice before copying anything from this repository. 


## Local development

Start the local app with:

    ./gradlew bootRun
    
The server will now be running on [localhost:8080](http://localhost:8080/).

The server will start with some basic data for testing. Two users will be created:  

 username | password 
----------|----------
 admin    | password 
 user     | user     


## Start docker container

To run the app you first need to setup a local mysql server. We have created a docker compose file
that should automate everything for you.

    $ ./gradlew build
    $ docker-compose build
    $ docker-compose up
    
The server will now be running on [localhost:8088](http://localhost:8088/).



