#!/usr/bin/env sh

mount //files/ctf /var/images -t cifs -o user=root,pass=ctf

nginx -c /etc/nginx/nginx.conf
