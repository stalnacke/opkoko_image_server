#!/usr/bin/env sh

mount //files/ctf /var/images -t cifs -o user=root,pass=ctf,uid=999

sudo -u opkoko_images -E java -Djava.security.egd=file:/dev/./urandom -jar /app.jar
